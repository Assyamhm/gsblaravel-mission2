<?php 


use App\Post;
use PDF;

public function getPostPdf (Post $post)
{
    // L'instance PDF avec la vue resources/views/posts/show.blade.php
    $pdf = PDF::loadView('posts.show', compact('post'));

    // Lancement du téléchargement du fichier PDF
    return $pdf->download(\Str::slug($post->title).".pdf");
    
}