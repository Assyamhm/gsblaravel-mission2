    <?php $__env->startSection('menu'); ?>
            <!-- Division pour le sommaire -->
        <div id="menuGauche">
            <div id="infosUtil">
                  
             </div>  
               <ul id="menuList">
                   <li >
                    <strong>Bonjour admin</strong>
                      
                   </li>
                  <li class="smenu">
                     <a href="<?php echo e(route('chemin_ajouterSaisie')); ?>" title="Ajouter un visiteur">Ajouter un visiteur</a>
                  </li>
                  <li class="smenu">
                    <a href="<?php echo e(route('chemin_modifierChoix')); ?>" title="Modifier un visiteur">Modifier un visiteur</a>
                  </li>
                  <li class="smenu">
                     <a href="<?php echo e(route('chemin_lister')); ?>" title="Afficher la liste des visiteurs">Liste des visiteurs</a>
                  </li>
               <li class="smenu">
                <a href="<?php echo e(route('chemin_deconnexion')); ?>" title="Se déconnecter">Déconnexion</a>
                  </li>
                </ul>
               
        </div>
    <?php $__env->stopSection(); ?>          
<?php echo $__env->make('modeles/visiteur', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH H:\laragon+\www\gsbLaravel\resources\views/sommaireAdmin.blade.php ENDPATH**/ ?>